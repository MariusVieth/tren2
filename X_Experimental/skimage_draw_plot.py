import numpy as np
from skimage.draw import line

import matplotlib.pyplot as plt

def sk_plot(data, w, h, datax=None, canvas=None, v_lines=[], h_lines=[], draw_axis=True, draw_data=True):
    if canvas is None:
        canvas = np.zeros((h, w), dtype=np.uint8)

    x_gap=10
    y_gap=10

    if datax is None:
        datax=range(len(data))

    x_old=0
    y_old=int(data[0])
    if draw_data:
        for i, d in zip(datax, data):
            x = int((w - 2 * x_gap) / np.max(datax) * i)
            y = int((h - 2 * y_gap) / np.max(data) * d)
            rr, cc = line(h - y_gap - 1 - y_old, x_gap + x_old, h - y_gap - 1 - y, x_gap + x)  # _aa
            canvas[rr, cc] = 125
            x_old = x
            y_old = y

    if draw_axis:
        rr, cc = line(y_gap, x_gap, h-y_gap-1, x_gap)
        canvas[rr, cc] = 255

        rr, cc = line(h-y_gap-1, x_gap, h-y_gap-1, w-x_gap-1)
        canvas[rr, cc] = 255

    for hl in h_lines:
        y = int((h-2*y_gap)/np.max(data)*hl)
        rr, cc = line(h - y_gap -1 -y, x_gap, h - y_gap -1-y, w-x_gap-1) #_aa
        canvas[rr, cc] = 255

    for vl in v_lines:
        x = int((w-2*x_gap)/np.max(datax)*vl)
        rr, cc = line(y_gap, x_gap+x, h-y_gap-1, x_gap+x) #_aa
        canvas[rr, cc] = 255

    return canvas

    #plt.imshow(canvas)
    #plt.show()


#plot(np.random.rand(100), 300, 300, h_lines=[0.5], v_lines=[50])
