from __future__ import unicode_literals
import sys
import random
import matplotlib
matplotlib.use('Qt5Agg')
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QLabel, QSlider

from numpy import arange, sin, pi
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from X_Experimental.Functions import *

from multiprocessing import Process, Queue, Value

import time



timesteps = 1000



def f(q,i,rest, reset, threshold, exc_rest, inh_rest, tau, tau_e, tau_i):
    while True:
        excitatory_input_streams = get_input_stream(timesteps, nr=50, rate_Hz=20)
        weighted_excitatory_input_streams = get_weighted_input_stream(excitatory_input_streams, weight_mul=0.04)

        inhibitory_input_streams = get_input_stream(timesteps, nr=10, rate_Hz=20)
        weighted_inhibitory_input_streams = get_weighted_input_stream(inhibitory_input_streams, weight_mul=0.01)

        exc_input = np.sum(weighted_excitatory_input_streams, axis=1)
        inh_input = np.sum(weighted_inhibitory_input_streams, axis=1)

        glu_levels = SpikeTrain_Glutamate(exc_input, tau_e.value)
        gaba_levels = SpikeTrain_GABA(inh_input, tau_i.value)
        volt_levels, spike_train = InputStream_Voltage(glu_levels, gaba_levels, rest.value,reset.value,threshold.value,exc_rest.value,inh_rest.value,tau.value)

        isi = SpikeTrain_ISI(spike_train)
        isi_hz = ISI_Hz(isi)

        q.put(isi_hz)

x=step_ms(np.array(range(1000)))

cores=3

rest = Value('d', -60)
reset = Value('d', -60)
threshold = Value('d', -57)
exc_rest = Value('d', 0.0)
inh_rest = Value('d', -80)
tau = Value('d', 20)
tau_e = Value('d', 3)
tau_i = Value('d', 5)


class MyMplCanvas(FigureCanvas):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)

        self.compute_initial_figure()

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def compute_initial_figure(self):
        pass


class MyDynamicMplCanvas(MyMplCanvas):
    """A canvas that updates itself every second with a new plot."""

    def __init__(self, *args, **kwargs):
        MyMplCanvas.__init__(self, *args, **kwargs)

        self.queues = []
        self.processes = []
        for i in range(cores):
            q = Queue()
            p = Process(target=f, args=(q, i, rest, reset, threshold, exc_rest, inh_rest, tau, tau_e, tau_i,))
            p.start()
            self.queues.append(q)
            self.processes.append(p)

        # for i in range(cores):
        #   processes[i].join()


        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(100)

    def compute_initial_figure(self):
        self.axes.plot([0, 1, 2, 3], [1, 2, 0, 4], 'r')

    results = []

    #@QtCore.pyqtSlot()
    def start(self):
        print('a')
        time.sleep(10)
        print('b')
        print('sdafsgfd')

    #@QtCore.pyqtSlot()
    def update_figure(self):
        start = time.time()

        self.axes.cla()

        for i in range(cores):
            try:
                isi_hz = self.queues[i].get(timeout=1)
                self.results.append(list(isi_hz))
            except Exception as error:
                pass

        if len(self.results) > 10:
            for i in range(len(self.results) - 10):
                self.results.pop(0)

        # self.results.append(list(isi_hz))
        # print(isi_hz.shape)


        # results=np.vstack(results)
        # print(results.shape)
        # np.concatenate(results, axis=0)
        # print(results.shape)



        xrightlim = 150
        binwidth = xrightlim / 30
        bins = np.arange(0, xrightlim, binwidth)

        self.axes.hist(self.results, bins=bins, stacked=True)  #
        self.axes.set_xlabel('Hz')
        self.axes.set_ylabel('Nr')
        self.axes.set_xlim([0, xrightlim])
        # fl=results.flatten()
        # self.axes.axvline(x=np.mean(fl) + np.std(fl), color='green')
        # self.axes.axvline(x=np.mean(fl) - np.std(fl), color='green')
        # self.axes.axvline(x=np.mean(fl), color='red')
        self.draw()

        print(time.time()-start)

        # class MyStaticMplCanvas(MyMplCanvas):
        #    def compute_initial_figure(self):
        #        self.axes.hist(isi_hz, bins=20)
        #        self.axes.set_xlabel('Hz')
        #        self.axes.set_ylabel('Nr')
        #        self.axes.set_xlim([0, 100])
        #        self.axes.axvline(x=np.mean(isi_hz) + np.std(isi_hz), color='green')
        #        self.axes.axvline(x=np.mean(isi_hz) - np.std(isi_hz), color='green')
        #        self.axes.axvline(x=np.mean(isi_hz), color='red')

        # self.axes.plot(x, glu_levels)





class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.init_QT_Window('test')
        self.main_widget = QtWidgets.QWidget(self)

        l = QtWidgets.QVBoxLayout(self.main_widget)

        class Worker(QtCore.QObject):#FigureCanvas

            def __init__(self):
                super().__init__()
                self.timer = QtCore.QTimer(self)
                self.timer.timeout.connect(self.start)
                self.timer.start(100)


            @QtCore.pyqtSlot()
            def start(self):
                print('a')
                time.sleep(10)
                print('b')
                #print('sdafsgfd')




        #Worker()
        self.my_canvas=MyDynamicMplCanvas(self.main_widget, width=5, height=4, dpi=100)
        #self.worker_thread = QtCore.QThread()
        #self.my_canvas.moveToThread(self.worker_thread)
        #self.worker_thread.start()

        l.addWidget(self.my_canvas)
        l.addWidget(QSlider(self))




        #self.worker = Worker()
        #self.worker_thread = QtCore.QThread()
        #self.worker.moveToThread(self.worker_thread)
        #self.worker_thread.start()

        #timer = QtCore.QTimer(self)
        #timer.timeout.connect(self.test)
        #self.worker.start()


        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

    def init_QT_Window(self, label):
        self.width = 1500
        self.height = 800
        self.sidebar_width = 300
        self.btn_height = 20
        self.setWindowTitle(label)
        self.setGeometry(10, 10, self.width, self.height)






'''

class MyStaticMplCanvas(MyMplCanvas):
    """Simple canvas with a sine plot."""

    def compute_initial_figure(self):
        t = arange(0.0, 3.0, 0.01)
        s = sin(2*pi*t)
        self.axes.plot(t, s)


class MyDynamicMplCanvas(MyMplCanvas):
    """A canvas that updates itself every second with a new plot."""

    def __init__(self, *args, **kwargs):
        MyMplCanvas.__init__(self, *args, **kwargs)
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(100)

    def compute_initial_figure(self):
        self.axes.plot([0, 1, 2, 3], [1, 2, 0, 4], 'r')

    def update_figure(self):
        l = [random.randint(0, 10) for i in range(4)]
        self.axes.cla()
        self.axes.plot([0, 1, 2, 3], l, 'r')
        self.draw()

'''

if __name__ == '__main__':
    qApp = QtWidgets.QApplication(sys.argv)

    aw = ApplicationWindow()
    aw.show()
    sys.exit(qApp.exec_())