import numpy as np
import time

mat_float = np.random.rand(100, 200)

inp_vec_bool = (np.random.rand(200) <= 0.04)
inp_vec_float = inp_vec_bool.copy() * 1.0

    #start = time.time()
    #activity = np.where(vec_bool,mat_float)
    #print(activity.shape)
    #activity = np.sum(mat_float[vec_bool], axis=0)
    #print(activity.shape)
    #print('test', time.time()-start)



start = time.time()
for _ in range(1):
    activity = np.sum(mat_float[:,inp_vec_bool], axis=1)
    print(activity)
print('bool', time.time()-start)

start = time.time()
for _ in range(1):
    activity = np.dot(mat_float, inp_vec_float)
    print(activity)
print('float', time.time() - start)

start = time.time()
for _ in range(1):
    activity = np.sum(mat_float.transpose()[inp_vec_bool], axis=0)
    print(activity)
print('boolt', time.time()-start)
