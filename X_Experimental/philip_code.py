import numpy as np
import matplotlib.pyplot as plt

ms_per_step = 2

timesteps = 10000

exc_input_nr = 100
inh_input_nr = 10

exc_weights = 2
inh_weights = 0.1

#rechnet array zeitschritte in ms um, da ein spike idr. länger ist als eine millisekunde (eher 2-3)
#wenn du annimmst, dass ein spike eine millisekunde ist brauchst du die funktion nicht
def step_ms(x):
    return x*ms_per_step

#berechnet den inter spike interval (zeit zwischen den spikes)
def SpikeTrain_ISI(x, step_to_ms=True):
    result = []
    last = -1
    for i, x in enumerate(x):
        if x == 1:
            result.append(i-last)
            last=i
    if step_to_ms:
        return step_ms(np.array(result))
    else:
        return np.array(result)

#inter spike interval zu herz umrechnen (für feuerrate)
def ISI_Hz(x):
    return 1/(x/1000)

#generiert die spikes der input neurone
def get_input_stream(steps, neurons, rate_Hz=20):
    return (np.random.rand(steps, neurons) < (rate_Hz * (ms_per_step / 1000))).astype(np.float32)

#gewichtet die spikes der input neurone mit zufälligen gewichten
def get_weighted_input_stream(x, weight_norm):
    return x * np.random.rand(x.shape[1]) / x.shape[1] * weight_norm

#generiert transmitterkonzentrationen an den sysnapsen, die bei jedem input spike höher werden und langsam wieder abfallen
def SpikeTrain_Transmitter(x, tau):
    result=[]
    transmitter=0
    for spike_value in x:
        transmitter += spike_value
        transmitter += -transmitter / tau
        result.append(transmitter)
    return np.array(result)

def SpikeTrain_Glutamate(x, tau_e=3):
    return SpikeTrain_Transmitter(x, tau_e)

def SpikeTrain_GABA(x, tau_i=5):
    return SpikeTrain_Transmitter(x, tau_i)

#berechnet die Spannung des Neurons und die output spikes
def InputStream_Voltage(exc, inh, rest=-60, reset=-60, threshold=-57, exc_rest=0.0, inh_rest=-80, tau=20):
    voltages = []
    spikes = []

    volt = rest
    for glu_level, gaba_level in zip(exc, inh):

        #differentialgleichung für spannung
        #erstet term: zieht spannung zu rest(-60)
        #zweiter: zieht spannung zu 0
        #dritter: zieht spannung zu -80
        volt += ((rest-volt) + glu_level*(exc_rest-volt) + gaba_level*(inh_rest-volt))/tau

        #spike event...
        if volt > threshold:
            spikes.append(1.0)
            volt = reset
        else:
            spikes.append(0.0)

        voltages.append(volt)

    return np.array(voltages), np.array(spikes)




excitatory_input_streams = get_input_stream(timesteps, neurons=exc_input_nr)
weighted_excitatory_input_streams = get_weighted_input_stream(excitatory_input_streams, weight_norm=exc_weights)

inhibitory_input_streams = get_input_stream(timesteps, neurons=inh_input_nr)
weighted_inhibitory_input_streams = get_weighted_input_stream(inhibitory_input_streams, weight_norm=inh_weights)

exc_input = np.sum(weighted_excitatory_input_streams, axis=1)
inh_input = np.sum(weighted_inhibitory_input_streams, axis=1)

glu_levels = SpikeTrain_Glutamate(exc_input)
gaba_levels = SpikeTrain_GABA(inh_input)


volt_levels, spike_train = InputStream_Voltage(glu_levels, gaba_levels)



inter_spike_intervals_ms=SpikeTrain_ISI(spike_train, True)
firing_rates=ISI_Hz(inter_spike_intervals_ms)

plt.hist(firing_rates, bins=20)
plt.show()


plt.plot(volt_levels[0:1000])
plt.axhline(-60)
plt.axhline(0)
plt.axhline(-80)
plt.show()