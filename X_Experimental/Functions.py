import numpy as np
#import matplotlib.pyplot as plt

def relu3(x ,param, stepheight):
    return np.clip((x > param) * (stepheight + (1-stepheight) / (1 - param) * (x - param)), 0, 1)

def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))



def Current_FiringRate(x):
    return relu3(x, 0.3, 0.0)

def Glutamate_FiringRate(x):
    return Current_FiringRate(Glu_Current(x))

def Glu_Na(x):
    return np.tanh(x)

def Glu_Ca(x):
    return x/10+np.tanh(relu3(x, 0.3, 0.0))

def Ca_LTP(x):
    return sigmoid(x, 10, -5)
    #return np.tanh(x*2)

def Ca_LTD(x):
    return sigmoid(x, 30, -5)/2
    #return np.tanh(x*10)/2

def Ca_STDP(x):
    return Ca_LTP(x)-Ca_LTD(x)

def Glu_Current(x):
    return Glu_Na(x)+2*Glu_Ca(x)


def temporal_difference(x):
    return gaussian((np.power(4,x*5)-1)/3+x, 0.4, 0.3)

def temporal_STDP(x):
    return Ca_STDP(temporal_difference(x))

def temporal_STDP2(x,a):
    return Ca_STDP(np.power(temporal_difference(x),a))

#def Ca_CAMKIV(x):
#    return np.tanh(relu3(x, 0.3, 0.0))

def SpikeTrain_Transmitter(x, tau):
    result=[]
    transmitter=0
    for spike_value in x:
        transmitter += spike_value
        transmitter += -transmitter / tau
        result.append(transmitter)
    return np.array(result)

def SpikeTrain_Glutamate(x, tau_e=3):
    return SpikeTrain_Transmitter(x, tau_e)

def SpikeTrain_GABA(x, tau_i=5):
    return SpikeTrain_Transmitter(x, tau_i)



def InputStream_Voltage_learning(gaba, exc_raw, weights, tau_e=3, rest=-60, reset=-60, threshold=-57, exc_rest=0.0, inh_rest=-80, tau=20, taupre=20, taupost=20, Aplus=0.0003, Aminus=-0.0001,ATotalMax = 0.4):
    voltages = []
    spikes = []

    Apre = SpikeTrain_Transmitter(exc_raw*Aplus, taupre)

    weights = weights / np.sum(weights) * ATotalMax

    volt = rest
    glu_level = 0
    apost = 0
    for raw_input, gaba_level, apre in zip(exc_raw, gaba, Apre):

        #print(raw_input.shape)

        glu_level += np.sum(raw_input*weights)
        glu_level += -glu_level / tau_e

        volt += ((rest-volt) + glu_level*(exc_rest-volt) + gaba_level*(inh_rest-volt))/tau
        apost += -apost/taupost

        weights += apost*raw_input

        if volt > threshold:
            spikes.append(1.0)
            volt = reset
            apost += Aminus

            weights += apre

        else:
            spikes.append(0.0)

        weights=weights/np.sum(weights)*ATotalMax

        voltages.append(volt)

    return np.array(voltages), np.array(spikes), weights





def InputStream_Voltage(inh, exc, rest=-60, reset=-60, threshold=-57, exc_rest=0.0, inh_rest=-80, tau=20):
    voltages = []
    spikes = []

    volt = rest
    for glu_level, gaba_level in zip(inh, exc):
        volt += ((rest-volt) + glu_level*(exc_rest-volt) + gaba_level*(inh_rest-volt))/tau

        if volt > threshold:
            spikes.append(1.0)
            volt = reset
        else:
            spikes.append(0.0)

        voltages.append(volt)

    return np.array(voltages), np.array(spikes)

def get_sliding_average_stream(x, delay_factor=100):
    result=[]
    avg=0.0
    for d in x:
        avg=(avg*delay_factor+d)/(delay_factor+1)
        result.append(avg)
    return np.array(result)


def SpikeTrain_ISI(x, step_to_ms=True):
    result = []
    last = -1
    for i, x in enumerate(x):
        if x == 1:
            if last!=-1:
                result.append(i-last)
            last=i
    if step_to_ms:
        return step_ms(np.array(result))
    else:
        return np.array(result)


def ISI_Hz(x):
    return 1/(x/1000)

ms_per_step=2

def step_ms(x):
    return x*ms_per_step

def ms_steps(x):
    return x/ms_per_step

def get_input_stream(steps, nr, rate_Hz):
    return (np.random.rand(steps, nr) < (rate_Hz * (ms_per_step / 1000))).astype(np.float32)

def get_weighted_input_stream(x, weight_mul):
    return x * np.random.rand(x.shape[1]) / x.shape[1] * weight_mul

def get_weighted_input_stream3(x, weight_mul, pattern_size=10):
    weights = np.random.rand(x.shape[1]) / x.shape[1] * weight_mul
    weights[0:pattern_size] += 1 / x.shape[1] * weight_mul
    weights[pattern_size:] *= 0.5
    return x * weights

def correlate(x, pattern_size=10):
    for i in range(len(x)):
        if i%10 == 0:
            rnd=np.random.rand() < 0.01
        x[i][0:pattern_size] *= rnd *10#np.sum(x[i][0:pattern_size])
    return x

def smooth(x):
    return np.array([(x[i]+x[i+1])/2.0 for i in range(len(x)-1)])


################################################


#np.random.normal(weight_mul/2,weight_mul/2*scale,x.shape[1])
def get_weighted_input_stream2(x, weight_mul, scale):
    return x * np.random.rand(x.shape[1]) / x.shape[1] * weight_mul #(np.random.rand(x.shape[1]) > scale)

def sigmoid(x,a,b):
    return 1/(1+np.exp(-(a*x+b)))

def sigmoid2(x,a,b):
    return 1/(1+np.exp(-(a*(x+b))))

def get_TREN_input_stream(steps, activator):
    return np.array(activator.get_pattern_samples(steps)).swapaxes(0, 1)

def get_TREN_output_stream(x, pre_mul, exponent, post_mul, firetreshold, stephight):
    return np.clip(np.power(relu3(np.clip(x, 0, 1), firetreshold, stephight) * pre_mul, exponent), 0, 1) * post_mul

def get_TREN_output_stream2(x, pre_mul, exponent, post_mul, firetreshold, stephight, act_mul):
    result = []

    act = 0
    for val in x:
        act = act*act_mul
        act = np.clip(act+val, 0, 1)
        temp = np.clip(np.power(relu3(act, firetreshold, stephight) * pre_mul, exponent), 0, 1) * post_mul
        result.append(temp)

    return np.array(result)

# return np.clip(sigmoid(np.clip(x, 0, 1), a, b)*c,0,1)
#import matplotlib.pyplot as plt

#x=(np.array(range(100)))/100#
#plt.plot(x,relu3(x,0.1,0))
#plt.show()
