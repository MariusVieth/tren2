import pybullet
import pybullet_data
print(pybullet_data.getDataPath())

client=pybullet.connect(pybullet.GUI)
# without GUI:
#client=pybullet.connect(pybullet.DIRECT)
import numpy as np

#import matplotlib.pyplot as plt

pybullet.resetSimulation()


pybullet.setAdditionalSearchPath(pybullet_data.getDataPath())

plane = pybullet.loadURDF("plane.urdf")
#plane = pybullet.loadURDF("laikago.urdf")

pybullet.setGravity(0, 0, -9.8, physicsClientId=client)
#pybullet.setRealTimeSimulation(1, physicsClientId=client)
#import os
# this may take a while...
#os.system("git clone https://github.com/ros-industrial/kuka_experimental.git")

robot = pybullet.loadMJCF('mjcf/ant.xml')[0]


#print(pybullet.getJointStates(robot[0],))

#print(pybullet.getBasePositionAndOrientation(ant))

#robot = pybullet.loadURDF("laikago/laikago.urdf", basePosition=[0,0,1.5],baseOrientation=[1,0,0,1])
#robot = pybullet.loadURDF("humanoid/humanoid.urdf", basePosition=[0,0,5],baseOrientation=[1,0,0,1])
#samu=pybullet.loadURDF("samurai.urdf", basePosition=[0,-7,0],globalScaling=0.5)
#block=pybullet.loadURDF("cube_no_rotation.urdf", basePosition=[0,0,0.5], globalScaling=0)
#robot = pybullet.loadURDF("quadruped/quadruped.urdf", basePosition=[0,0,0.5],baseOrientation=[1,0,0,1])


def moveLeg(robot, id=0, position=0, force=1.5):
    pybullet.setJointMotorControl2(
        robot,
        id,
        pybullet.POSITION_CONTROL,
        targetPosition=position,
        force=force
        #maxVelocity=5
    )

#robot = pybullet.loadURDF("biped2d_pybullet.urdf", basePosition=[0,0,10])
#robot = pybullet.loadURDF("biped2d_pybullet.urdf", basePosition=[0,0,20])
#robot = pybullet.loadURDF("biped2d_pybullet.urdf", basePosition=[0,0,30])
#robot = pybullet.loadURDF("biped2d_pybullet.urdf", basePosition=[0,0,40])
import time

front=[True,True,False,False]
back=[False,False,True,True]

inner_shoulders=np.array([0,3,6,9])
outer_shoulders=np.array([1,4,7,10])
knees=np.array([2,5,8,11])

#for i in inner_shoulders:
#    moveLeg(robot, i, 0.4, 30)  #

#for i in outer_shoulders[front]:
#    moveLeg(robot, i, -0.4, 30)  #

#for i in outer_shoulders[back]:
#    moveLeg(robot, i, 0.0, 30)  #

camTargetPos = [0, 0, 0.5]
cameraUp = [0, 0, 1]
cameraPos = [1, 1, 1]

pitch = -10.0

roll = 0
upAxisIndex = 2
camDistance = 1
pixelWidth = 320
pixelHeight = 200
nearPlane = 0.01
farPlane = 100

fov = 60
aspect = pixelWidth / pixelHeight

state=pybullet.saveState()

counter=0
while True:
    counter+=1
    pybullet.stepSimulation(client)

    print(pybullet.getBasePositionAndOrientation(robot))

    if counter%1000==0:
        pybullet.restoreState(state)

    #if counter % 100 == 0:
    for i in range(20):
        moveLeg(robot, i, (np.random.rand()-0.5)*1, 3000)#

    #for yaw in range(0, 360, 10):
    #    viewMatrix = pybullet.computeViewMatrixFromYawPitchRoll(camTargetPos, camDistance, yaw, pitch, roll, upAxisIndex)

    #    projectionMatrix = pybullet.computeProjectionMatrixFOV(fov, aspect, nearPlane, farPlane)
    #    img_arr = pybullet.getCameraImage(pixelWidth,pixelHeight,viewMatrix,projectionMatrix,shadow=1,lightDirection=[1, 1, 1],renderer=pybullet.ER_BULLET_HARDWARE_OPENGL)

        #print(np.array(img_arr[2]).shape)

    #    plt.matshow(img_arr[2], cmap='gray')
    #    plt.show()

    #if counter % 100 == 0:
    #    for i in knees[front]:
    #        moveLeg(robot, i, -0.8, 30)#
    #    for i in knees[back]:
    #        moveLeg(robot, i, 0.8, 30)#

    #if counter % 50 == 10:
    #    for i in knees[front]:
    #        moveLeg(robot, i, 0.8, 30)#
    #    for i in knees[back]:
    #        moveLeg(robot, i, -0.8, 30)#


    time.sleep(0.01)