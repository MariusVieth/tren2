import numpy as np
from PIL import Image, ImageDraw,ImageFilter,ImageEnhance
from brian2 import *

def rotatearoundpoint(p, rot, middle):
    aa = p[0] - middle[0]
    bb = p[1] - middle[1]
    cc = np.sqrt(aa * aa + bb * bb)
    a = 0
    if cc != 0:
        a = np.rad2deg(np.arcsin(aa / cc))
    if bb < 0:
        a = 180 - a
    a = np.deg2rad(a + rot)
    aa = np.sin(a) * cc
    bb = np.cos(a) * cc
    return (middle[0] + aa, middle[1] + bb)


def picture_to_array(image, max=1):
    result = []
    for y in range(image.size[1]):
        for x in range(image.size[0]):
            val = image.getpixel((x, y))
            result.append(val)
            if val > max:
                max = val
    for i, val in enumerate(result):
        result[i] = val / max
    return np.array(result)

def show_array_pic(array, width, height):
    arr=array[0:width*height].reshape((width, height))
    plt.imshow(arr, cmap='hot', interpolation='nearest')
    plt.show()

def show_synapse_overview_pic(synapse_array, neuron_count, synapses_per_neuron,input_grid_width,input_grid_height,add_own_synapse,input_only=True):
    neuron_synapse_list=np.array(synapse_array)[0:neuron_count*synapses_per_neuron].reshape((neuron_count, synapses_per_neuron))

    border_value=np.max(neuron_synapse_list)

    if add_own_synapse:
        inserted = []
        synapses_per_neuron +=1
        for i,synapses in enumerate(neuron_synapse_list):
            inserted.append(np.insert(synapses,i,0))
        neuron_synapse_list=inserted

    if not input_only:
        input_grid_height=int(synapses_per_neuron/input_grid_width)

    dim=int(sqrt(neuron_count))

    result_pic_width=dim*(input_grid_width+1)
    result_pic_height=(dim+1)*(input_grid_height+1)

    result = np.ones((result_pic_height,result_pic_width))*border_value

    x=0
    y=0
    for synapses in neuron_synapse_list:
        inp_syn = synapses[0:input_grid_width * input_grid_height].reshape((input_grid_height, input_grid_width))
        result[y*(input_grid_height+1):y*(input_grid_height+1)+input_grid_height,x*(input_grid_width+1):x*(input_grid_width+1)+input_grid_width]=inp_syn
        x += 1
        if x>=dim:
            y += 1
            x=0

    plt.imshow(result, cmap='hot', interpolation='nearest')
    plt.show()


#def array_to_picture(array, width, height, max=1):
#    result = Image.new('L', (width, height), (0))
#    arrpic =
#    for y in range(image.size[1]):
#        for x in range(image.size[0]):
#            val = image.getpixel((x, y))
#            result.append(val)
#            if val > max:
#                max = val
#    for i, val in enumerate(result):
#        result[i] = val / max
#    return result

def getHorLinePicture(y,width,height):
    im = Image.new('L', (width, height), (0))
    draw = ImageDraw.Draw(im)
    draw.line((0, y, width-0, y), fill=255)
    return picture_to_array(im)

def getVerLinePicture(x,width,height):
    im = Image.new('L', (width, height), (0))
    draw = ImageDraw.Draw(im)
    draw.line((x, 0, x, height-0), fill=255)
    return picture_to_array(im)



def getRotLinePicture(deg, width, height):
    im = Image.new('L', (width, height), (0))
    draw = ImageDraw.Draw(im)
    rot_point = rotatearoundpoint((width / 2, 0), deg, (0, 0))
    draw.line(
        (width / 2 - rot_point[0], height / 2 - rot_point[1], width / 2 + rot_point[0], height / 2 + rot_point[1]),
        fill=255)
    im.save("{}.jpg".format(deg), "JPEG")
    return picture_to_array(im)


def generate_patterns(width, height):
    result=[]
    for i in range(width):
        for j in range(height):
            result.append(np.clip(getHorLinePicture(i,width,height)+getVerLinePicture(j,width,height),0,1))
    return result
    #show_array_pic(getRotLinePicture(10),5,5)
    #return [getRotLinePicture(10, width, height), getRotLinePicture(70, width, height)]
    #return [[1,1,1,0,0],[1,0,1,0,1]]


def get_combined_pattern_timeline_array(neuron_count, width, height, timesteps, show_duration):

    #th = 0.2
    patterns=generate_patterns(width, height)

    #print(patterns)

    timeline = []
    for i in range(int(timesteps/show_duration)):#np.random.rand(timesteps):

        pattern_nr = np.random.randint(len(patterns))

        for d in range(show_duration):
            combined_pattern=np.zeros(neuron_count)
            #for rnd, pattern in zip(np.random.rand(len(patterns)), patterns):
                #if rnd < th:
                #    combined_pattern[0:len(pattern)] += pattern


            #pattern_nr=(i%2)*22
            #pattern_nr = 0
            pattern=patterns[pattern_nr]
            combined_pattern[0:len(pattern)] += pattern
            combined_pattern=np.clip(combined_pattern, 0, 1)
            timeline.append(combined_pattern)
            #show_array_pic(pattern,width, height)

    return timeline

    #print(timeline)
