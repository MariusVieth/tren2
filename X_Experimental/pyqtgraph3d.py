# -*- coding: utf-8 -*-
"""
    Animated 3D sinc function
"""

from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.opengl as gl
import pyqtgraph as pg
import numpy as np
import sys


class Visualizer(object):
    def __init__(self, data):
        self.data=data
        self.traces = dict()
        self.app = QtGui.QApplication(sys.argv)
        self.w = gl.GLViewWidget()
        self.w.opts['distance'] = 40
        self.w.setWindowTitle('pyqtgraph example: GLLinePlotItem')
        self.w.setGeometry(0, 110, 1920, 1080)
        self.w.show()

        # create the background grids
        gx = gl.GLGridItem()
        gx.rotate(90, 0, 1, 0)
        gx.translate(-10, 0, 0)
        self.w.addItem(gx)
        gy = gl.GLGridItem()
        gy.rotate(90, 1, 0, 0)
        gy.translate(0, -10, 0)
        self.w.addItem(gy)
        gz = gl.GLGridItem()
        gz.translate(0, 0, -10)
        self.w.addItem(gz)

        self.n = 50
        self.m = 1000
        self.y = np.linspace(-100, 100, self.n)
        self.x = np.linspace(-100, 100, self.m)
        self.phase = 0

        center_x=0
        center_y=5
        center_z=10

        step=10
        for i in range(step):
            z=i*2
            ys = np.random.normal(0, 1, 100)#self.data[i*step:(i+1)*step]
            hist, bins = np.histogram(ys, bins=10)

            z=np.ones(hist.shape[0])*z-center_z
            x=bins*10.0-center_x
            y=hist*0.5-center_y

            #pts = []
            for k,l,m in zip(z, x, y):
                #print(k,l,m)
                #pts.append([k, l, m])
                bar=gl.GLBarGraphItem([k,l], m)
                self.w.addItem(bar)

            #pts=np.array(pts)
            #pts = np.hstack([np.array(a), np.array(b), np.zeros(a.shape[0])])

            #self.traces['a'] =gl.GLSurfacePlotItem(z,x,y)
            #gl.GLLinePlotItem(pos=pts, color=(np.random.rand(),np.random.rand(),np.random.rand(),1), width=10,antialias=True)
            #self.w.addItem(self.traces['a'])

        #curve = pg.PlotCurveItem(x, y, stepMode=True, fillLevel=0, brush=(0, 0, 255, 80))



        #for i in range(self.n):
        #    yi = np.array([self.y[i]] * self.m)
        #    d = np.sqrt(self.x ** 2 + yi ** 2)
        #    z = 10 * np.cos(d + self.phase) / (d + 1)
        #    pts = np.vstack([self.x, yi, z]).transpose()
        #    #print(self.x.shape, yi.shape, np.zeros(z.shape).shape)
        #    #print(pts)
        #    self.traces[i] = gl.GLLinePlotItem(pos=pts, color=pg.glColor((i, self.n * 1.3)), width=(i + 1) / 10, antialias=True)
        #    self.w.addItem(self.traces[i])

    #def start(self):
    #    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
    #        QtGui.QApplication.instance().exec_()

    #def set_plotdata(self, name, points, color, width):
    #    self.traces[name].setData(pos=self.data, color=color, width=width)

    #def update(self):
    #    for i in range(self.n):
    #        yi = np.array([self.y[i]] * self.m)
    #        d = np.sqrt(self.x ** 2 + yi ** 2)
    #        z = 10 * np.cos(d + self.phase) / (d + 1)
    #        pts = np.vstack([self.x, yi, z]).transpose()
    #        self.set_plotdata(
    #            name=i, points=pts,
    #            color=pg.glColor((i, self.n * 1.3)),
    #            width=(i + 1) / 10
    #        )
    #        self.phase -= .003

    #def animation(self):
    #    timer = QtCore.QTimer()
    #    timer.timeout.connect(self.update)
    #    timer.start(20)
    #    self.start()



# Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    v = Visualizer(np.random.normal(0.0,1.0,10000))
    sys.exit(v.app.exec_())
#v.animation()