from PIL import Image, ImageDraw, ImageFilter, ImageEnhance
from scipy.ndimage import *
from scipy import signal
import scipy.ndimage.filters as fi
import numpy as np
import matplotlib.pyplot as plt


pil_image = Image.open('../Data/Images/pexels-photo-275484.jpeg')
pil_image_gray = pil_image.convert('L')
image_rgb = np.array(pil_image).astype(np.float32)
image_gray=image_rgb[:, :, 0]+image_rgb[:, :, 1]+image_rgb[:, :, 2]


def get_receptive_field_resort_mask(w, h, tw, th, sharpness=1.5):
    result_img=np.zeros((w, h))

    for x in range(w):
        for y in range(h):
            dx = w/2-x
            dy = h/2-y
            dist = np.sqrt(dx*dx+dy*dy)
            d = np.clip(1-(dist)/(w/2), 0, None)
            result_img[x, y] = np.maximum(np.power(d, sharpness), 0.0000001)

    rif = result_img.flatten()
    rif = rif/sum(rif)
    result = np.random.choice(np.array(list(range(rif.shape[0]))), tw*th, False, rif)

    return result_img, result


def compress_receptive_field(patch, resort_mask):
    print(patch.shape,resort_mask.shape)
    return patch.flatten()[resort_mask]

def decompress_receptive_field(patch, resort_mask, upscale_size):
    f_pic = np.zeros((upscale_size)).flatten()
    f_pic[resort_mask] = patch
    return f_pic



def gkern(kernlen=9, nsig=2):
    print(kernlen, nsig)
    inp = np.zeros((kernlen, kernlen))
    inp[kernlen//2, kernlen//2] = 1
    return fi.gaussian_filter(inp, nsig)


def fill_gaps_simple(image):
    decomp_img = image / np.max(image)
    blured = signal.convolve2d(image, gkern(int(np.max(image.shape)/10), np.maximum(np.max(image.shape)/50,1)), boundary='symm', mode='same')
    blured = blured / np.max(blured) #* 1.3
    return decomp_img + blured * (decomp_img == 0)


def fill_gaps(image):
    result=image.copy()
    for x in range(image.shape[0]):
        for y in range(image.shape[1]):

            if result[x,y]==-1:
                surround_patch=image[np.maximum(x-1,0):np.minimum(x+1,image.shape[0]),np.maximum(y-1,0):np.minimum(y+1,image.shape[1])].copy()
                surround_patch_valid_pixels=surround_patch>=0
                if sum(surround_patch_valid_pixels)>0:
                    surround_patch=surround_patch*surround_patch_valid_pixels
                    result[x,y]=int(sum(surround_patch)/sum(surround_patch_valid_pixels))
    return result


w = 275
h = 275
upscale = 5.0
wup=int(w*upscale)
hup=int(h*upscale)

pic, re = get_receptive_field_resort_mask(wup, hup, w, h, sharpness=10)

comp_pic = compress_receptive_field(image_gray[0:0+wup, 0:0+hup], re)
decomp_pic = decompress_receptive_field(comp_pic, re, wup*hup)
decomp_img=decomp_pic.reshape(wup, hup)
decomp_img=fill_gaps_simple(decomp_img)

#filled=fill_gaps_simple(decomp_pic.reshape(wup, hup))

#filled=filled+(filled<0)

plt.imshow(decomp_img, cmap=plt.cm.gist_gray, interpolation='nearest')
plt.show()