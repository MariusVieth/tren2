from __future__ import unicode_literals
import sys
import random
#import matplotlib
#matplotlib.use('Qt5Agg')
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QLabel, QSlider, QCheckBox, QLineEdit

from numpy import arange, sin, pi
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

#from X_Experimental.Functions import *
from multiprocessing import Process, Queue, Value
import time
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg
from PyQt5.QtCore import Qt

from X_Experimental.pattern_generator import *

weight=Value('d', 1)

alpha=Value('d', 1)
beta=Value('d', 1)
gamma=Value('d', 1)
p_var=Value('d', 1)

sigma1=Value('d', 1)
sigma2=Value('d', 1)

def f(q, weight, alpha, beta, gamma, p_var, sigma1, sigma2):

    while True:
        if q.empty():
            #print(weight.value)
            timesteps =10000
            data = []
            wv = weight.value

            # decay anti drifting * increase drifting + stabilization
            #w = w - beta_rnd[i]*w + p_rnd[i]*gamma_rnd[i]*w + alpha_rnd[i]*(wv-w)

            #r=np.random.rand(timesteps)-0.5
            #r=r*alpha.value

            #for i in range(timesteps):
            #    w = wv
            #    w += r[i]
            #    w = np.clip(w, 0, 10)
            #    data.append(w)

            ##################################################
            ##################################################
            ##################################################

            #l=numpy.random.lognormal(mean=wv, sigma=sigma1.value/10, size=(timesteps))

            alpha_rnd = np.random.rand(timesteps) * wv * (beta.value-gamma.value)#* alpha.value
            beta_rnd = np.random.rand(timesteps) * beta.value
            gamma_rnd = np.random.rand(timesteps) * gamma.value
            p_rnd = np.random.rand(timesteps) * p_var.value

            w = wv
            for i in range(timesteps):
                #w = w - beta_rnd[i]*w + p_rnd[i] * (alpha_rnd[i] + gamma_rnd[i]*w)
                w = w + (-beta_rnd[i]+gamma_rnd[i]) * w + (alpha_rnd[i])#
                #w=l[i]
                #w = w + (-beta_rnd[i] + gamma_rnd[i]) * w + alpha_rnd[i]
                w = np.clip(w, 0, 15)
                data.append(w)


            ##################################################
            ##################################################
            ##################################################

            #n1=np.random.normal(loc=(1-beta.value+p_var.value*gamma.value), scale=sigma1.value/10, size=(timesteps))
            #n2=np.random.normal(loc=(p_var.value*alpha.value), scale=sigma2.value/10, size=(timesteps))

            #w = wv
            #for i in range(timesteps):

                #w = n1[i]*w + n2[i]

                #w = w - beta_rnd[i]*w + p_rnd[i]*gamma_rnd[i]*w + alpha_rnd[i]*(wv-w)#beta_rnd[i] + p_rnd[i] * (alpha_rnd[i] + gamma_rnd[i] )
                #w = np.clip(w, 0, 15)
                #data.append(w)

            q.put(data)

cores = 3

class Plot_Surface(QtGui.QWidget):
    #results = []

    def __init__(self):
        super().__init__()

        self.setLayout(QtGui.QVBoxLayout())
        self.canvas = pg.GraphicsLayoutWidget()
        self.layout().addWidget(self.canvas)

        self.otherplot = self.canvas.addPlot(row=0, col=0)


    def update(self, data_stream):

        self.otherplot.setXRange(-5, 15, padding=0)
        #self.otherplot.setYRange(0, 1000, padding=0)

        ## make interesting distribution of values
        #vals = np.hstack([np.random.normal(size=500), np.random.normal(size=260, loc=4)])

        self.otherplot.clear()

        #for result in self.results:
        data = [item for sublist in data_stream for item in sublist]


        if len(data)>0:
            y, x = np.histogram(data, bins=50)
            curve = pg.PlotCurveItem(x, y, stepMode=True, fillLevel=0, brush=(0, 0, 255, 80))
            self.otherplot.addItem(curve)

        div = (beta.value-p_var.value*gamma.value)
        if div != 0:
            mean=p_var.value/div*alpha.value
            self.mean_line = pg.InfiniteLine(angle=90, pos=mean, movable=False, pen=[255, 0, 0, 255])
            self.otherplot.addItem(self.mean_line, ignoreBounds=True)



class ApplicationWindow(QtWidgets.QMainWindow):

    slider_resolution = 1000

    def set_slider(self, slider, value):
        slider.setValue(value*self.slider_resolution)
        slider.label.setText(slider.text + ' {:.3f}'.format(value))
        slider.target.value = value

    def on_released(self):
        slider=self.sender()
        current_value = slider.value() / self.slider_resolution
        slider.target.value = current_value
        slider.label.setText(slider.text+' {:.3f}'.format(current_value))

    def add_slider_label(self, box_layout, name, min, max, target, add_onclick=True):

        label=QLabel()
        label.setText(name)

        slider = QSlider(Qt.Horizontal)
        slider.setMinimum(min*self.slider_resolution)
        slider.setMaximum(max*self.slider_resolution)
        val=min
        if target is not None:
            val=target.value
        slider.setValue(val * self.slider_resolution)
        #slider.resize(self.sidebar_width, 5)
        if add_onclick:
            slider.valueChanged.connect(self.on_released)

        slider.target = target
        slider.label=label
        slider.text=name

        label.setText(slider.text + ' {}'.format(val))

        box_layout.addWidget(label)
        box_layout.addWidget(slider)

        return slider

    def space(self, layout, number):
        for i in range(number):
            layout.addWidget(QLabel())

    def __init__(self):

        self.queues = []
        self.processes = []
        for i in range(cores):
            q = Queue()
            self.img_q = Queue()
            p = Process(target=f, args=(q, weight, alpha, beta, gamma, p_var, sigma1, sigma2))
            p.start()
            self.queues.append(q)
            self.processes.append(p)


        QtWidgets.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.init_QT_Window('test')
        self.main_widget = QtWidgets.QWidget(self)

        h=QtWidgets.QHBoxLayout(self.main_widget)


        l = QtWidgets.QVBoxLayout(self)
        self.add_slider_label(l, 'weight', 0, 10, weight)

        self.add_slider_label(l, 'alpha', 0, 10, alpha)
        self.add_slider_label(l, 'beta', 0, 10, beta)
        self.add_slider_label(l, 'gamma', 0, 10, gamma)
        self.add_slider_label(l, 'p_var', 0, 10, p_var)

        self.add_slider_label(l, 'sigma1', 0, 10, sigma1)
        self.add_slider_label(l, 'sigma2', 0, 10, sigma2)

        h.addLayout(l, stretch=1)


        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update)
        timer.start(30)


        self.my_canvas = Plot_Surface()
        h.addWidget(self.my_canvas, stretch=6)

        self.setCentralWidget(self.main_widget)

    data_stream=[]

    def update(self):

        c=0
        for q in self.queues:
            while not q.empty():
                c+=1
                data = q.get(timeout=1)
                self.data_stream.append(data)

        print(c)
        max_data=10

        if len(self.data_stream) > max_data:
            for i in range(len(self.data_stream) - max_data):
                self.data_stream.pop(0)


        self.my_canvas.update(self.data_stream)


    def sl1event(self):
        value=self.sl1.value()/self.slider_resolution

        th_add = 2.1*value
        reset_add = 5*value

        self.set_slider(self.reset_slider, -63 + reset_add)
        self.set_slider(self.threshold_slider, -58.5+th_add)

        self.sl1.label.setText(self.sl1.text+' {}'.format(value))


    def sl2event(self):
        value=self.sl2.value()/self.slider_resolution

        th_add = -5*value
        reset_add = 10*value

        self.set_slider(self.reset_slider, -63 + reset_add)
        self.set_slider(self.threshold_slider, -58.5 + th_add)

        self.sl2.label.setText(self.sl2.text + ' {}'.format(value))


    def init_QT_Window(self, label):
        self.width = 1500
        self.height = 500
        self.sidebar_width = 300
        self.btn_height = 20
        self.setWindowTitle(label)
        self.setGeometry(10, 10, self.width, self.height)

    def closeEvent(self, *args, **kwargs):
        for process in self.processes:
            process.terminate()

if __name__ == '__main__':
    qApp = QtWidgets.QApplication(sys.argv)

    aw = ApplicationWindow()
    aw.show()
    sys.exit(qApp.exec_())