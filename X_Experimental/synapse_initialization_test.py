import numpy as np
#import matplotlib.pyplot as plt

def get_distance_mat(src_x, src_y, dst_x, dst_y, radius):
    result_syn_mat=np.zeros((len(dst_x), len(src_x)))

    for d_n in range(len(dst_x)):
        dx = np.abs(src_x - dst_x[d_n])
        dy = np.abs(src_y - dst_y[d_n])

        dist = np.sqrt(dx * dx + dy * dy)
        inv_dist = np.clip(radius - dist, 0.0, None)
        inv_dist /= np.max(inv_dist)

        result_syn_mat[d_n] = inv_dist

    return result_syn_mat

width_src = 100
height_src = 100
size_src=width_src*height_src

px_src = np.array([i % width_src for i in range(size_src)]).astype(np.float32)
py_src = np.array([np.floor(i / width_src) % height_src for i in range(size_src)]).astype(np.float32)

width_dst = 100
height_dst = 100
size_dst = width_dst*height_dst

px_dst = np.array([i % width_dst for i in range(size_dst)]).astype(np.float32)
py_dst = np.array([np.floor(i / width_dst) % height_dst for i in range(size_dst)]).astype(np.float32)

dm=get_distance_mat(px_src, py_src, px_dst, py_dst, 50)

sm = np.clip(dm - np.power(dm, 8.0), 0.0, None) * np.random.rand(len(px_dst), len(py_dst))

plt.matshow(sm[5050].reshape(100, 100))
plt.show()
