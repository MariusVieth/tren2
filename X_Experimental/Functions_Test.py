from X_Experimental.Functions import *

x=np.array(range(100))/100

fig, axes = plt.subplots(5, 5)

def my_plot(ypos, xpos, x,y,x_label,y_label, color='blue'):
    axes[ypos, xpos].plot(x, y, color)
    axes[ypos, xpos].set_xlabel(x_label)
    axes[ypos, xpos].set_ylabel(y_label)
    return axes[ypos, xpos]

show_reaction_curves=True
if show_reaction_curves:
    my_plot(0,0,x,Glu_Na(x),'GLU','Na+')
    my_plot(0,1,x,Glu_Ca(x),'GLU','Ca2+')
    my_plot(0,2,x,Glu_Current(x),'GLU','Current')
    my_plot(0,3,x,Current_FiringRate(x),'current','firing rate')
    my_plot(0,4,x,Glutamate_FiringRate(x/2),'GLU','firing rate',color='red')

show_learning_curves=True
if show_learning_curves:
    my_plot(1,0,x,Ca_LTP(x),'Ca2+','LTP')
    my_plot(1,1,x,Ca_LTD(x),'Ca2+','LTD')
    my_plot(1,2,x,Ca_STDP(x),'Ca2+','STDP', color='red')
    my_plot(1,3,x-0.5,temporal_difference(x-0.5),'delta t','Ca2+')
    my_plot(1,4,x-0.5,temporal_STDP(x-0.5),'delta t','STDP', color='red')

    x2=x-0.5
    my_plot(2, 4, x2, temporal_STDP2(x2,2), 'delta t', 'STDP', color='red')


timesteps=100000

excitatory_input_streams = get_input_stream(timesteps, nr=50, rate_Hz=20)
weighted_excitatory_input_streams = get_weighted_input_stream(excitatory_input_streams, weight_mul=0.04)

inhibitory_input_streams = get_input_stream(timesteps, nr=10, rate_Hz=20)
weighted_inhibitory_input_streams = get_weighted_input_stream(inhibitory_input_streams, weight_mul=0.01)


exc_input = np.sum(weighted_excitatory_input_streams, axis=1)
inh_input = np.sum(weighted_inhibitory_input_streams, axis=1)

glu_levels=SpikeTrain_Glutamate(exc_input)
gaba_levels=SpikeTrain_GABA(inh_input)
volt_levels, spike_train=InputStream_Voltage(glu_levels, gaba_levels)

isi=SpikeTrain_ISI(spike_train)
isi_hz=ISI_Hz(isi)


x=step_ms(np.array(range(timesteps)))

show_levels=False
if show_levels:
    my_plot(3,0,x,exc_input,'t','spike', color='green').set_ylim([-0.1, 2])
    my_plot(3,1,x,inh_input,'t','spike', color='green').set_ylim([-0.1, 2])

    my_plot(4,0,x,glu_levels,'t','GLU', color='green')
    my_plot(4,1,x,gaba_levels,'t','GABA', color='green')



    my_plot(4, 2, x, volt_levels, 't','voltage', color='green').set_ylim([-70, -40])
    my_plot(4, 3, x, spike_train, 't','spike', color='green').set_ylim([-0.1, 2])

    my_plot(4,4,list(range(len(isi))),isi,'spike','ISI', color='green')

show_histograms=False
if show_histograms:
    axes[3, 3].hist(isi_hz, bins=20)
    axes[3, 3].set_xlabel('Hz')
    axes[3, 3].set_ylabel('Nr')
    axes[3, 3].set_xlim([0, 100])
    axes[3, 3].axvline(x=np.mean(isi_hz) + np.std(isi_hz), color='green')
    axes[3, 3].axvline(x=np.mean(isi_hz) - np.std(isi_hz), color='green')
    axes[3, 3].axvline(x=np.mean(isi_hz), color='red')

    axes[3, 4].hist(isi, bins=20)
    axes[3, 4].set_xlabel('ISI')
    axes[3, 4].set_ylabel('Nr')
    axes[3, 4].axvline(x=np.mean(isi) + np.std(isi), color='green')
    axes[3, 4].axvline(x=np.mean(isi) - np.std(isi), color='green')
    axes[3, 4].axvline(x=np.mean(isi), color='red')

show_spiketrains=False
if show_spiketrains:
    axes[2, 0].imshow(excitatory_input_streams.transpose(), aspect='auto', cmap='binary', extent=(0,step_ms(timesteps),0,excitatory_input_streams.shape[1]))
    axes[2, 0].set_xlabel('spikes')
    axes[2, 0].set_ylabel('neurons')

    axes[2, 1].imshow(inhibitory_input_streams.transpose(), aspect='auto', cmap='binary', extent=(0,step_ms(timesteps),0,inhibitory_input_streams.shape[1]))
    axes[2, 1].set_xlabel('spikes')
    axes[2, 1].set_ylabel('neurons')



plt.show()