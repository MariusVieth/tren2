import sys
import time
import threading

def input_collect_function():
    while True:
        for line in sys.stdin:
            if line and line!='\n':
                if '[' in line and ']' in line:
                    print('new individual')
                elif 'pause' in line:
                    print('evolution stopped')
                elif 'start' in line:
                    print('evolution started')
                elif line.replace('\r','').replace('\n','').isdigit():
                    print('thread count set to '+line)
                else:
                    print('unknown command'+line)
        time.sleep(1)

threading.Thread(target=input_collect_function).start()

while True:
    print('working...')
    time.sleep(10)
