from scipy import ndimage, misc
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw,ImageFilter,ImageEnhance
import numpy as np

ascent = misc.ascent()

print(ascent)
print(np.min(ascent), np.max(ascent), ascent.shape, type(ascent), ascent.dtype)

img = Image.open('../Data/Images/testimage.jpg')

rgb = np.array(img)

img_gray = img.convert('L')
img_gray = np.array(img_gray)



fig = plt.figure()
#plt.gray()  # show the filtered result in grayscale
ax1 = fig.add_subplot(4,3,1)
ax2 = fig.add_subplot(4,3,2)
ax3 = fig.add_subplot(4,3,3)
ax4 = fig.add_subplot(4,3,4)
ax5 = fig.add_subplot(4,3,5)
ax6 = fig.add_subplot(4,3,6)
ax7 = fig.add_subplot(4,3,7)
ax8 = fig.add_subplot(4,3,8)

ax9 = fig.add_subplot(4,3,9)
ax10 = fig.add_subplot(4,3,10)
ax11 = fig.add_subplot(4,3,11)

result = ndimage.gaussian_laplace(img_gray.astype(np.int32),  sigma=3)

print(result.shape)

ax1.imshow(result*(result > 0), cmap='gray')    #on center
ax2.imshow(result*(result < 0)*-1, cmap='gray') #off center

ax3.imshow(img_gray, cmap=plt.get_cmap('gray')) #off center
ax4.imshow(255-img_gray, cmap=plt.get_cmap('gray')) #off center

print(rgb.shape)

ax5.imshow(rgb) #off center

#print(ascent.shape)

ax6.imshow(rgb[:,:,0], cmap=plt.get_cmap('gray'), vmin=0, vmax=255)
ax7.imshow(rgb[:,:,1], cmap=plt.get_cmap('gray'), vmin=0, vmax=255)
ax8.imshow(rgb[:,:,2], cmap=plt.get_cmap('gray'), vmin=0, vmax=255)

ax9.imshow(255-rgb[:,:,0], cmap=plt.get_cmap('gray'), vmin=0, vmax=255)
ax10.imshow(255-rgb[:,:,1], cmap=plt.get_cmap('gray'), vmin=0, vmax=255)
ax11.imshow(255-rgb[:,:,2], cmap=plt.get_cmap('gray'), vmin=0, vmax=255)

plt.show()

