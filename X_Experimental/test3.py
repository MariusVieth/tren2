
from PIL import Image, ImageDraw,ImageFilter,ImageEnhance
from scipy.ndimage import *
#import matplotlib.pyplot as plt
import numpy as np

m1 = np.ones((4, 4))*1
m2 = np.ones((4, 4))*2

m3 = np.ones((4, 4))*3
m4 = np.ones((4, 4))*4

#m12 = np.dstack((m1, m2)).reshape((4, 2))#, 1, 0)
#m34 = np.dstack((m3, m4)).reshape((4, 2))#, 1, 0)

result = np.zeros((4,4))

for x in range(2):
    for y in range(2):
        x2=x*2
        y2=y*2
        #print('a',m1[y2:y2+2, x2:x2+2])
        print(x,y)
        result[y2, x2]=np.sum(m1[y2:y2+2,x2:x2+2])/4
        result[y2+1, x2]=np.sum(m2[y2:y2+2,x2:x2+2])/4
        result[y2, x2+1]=np.sum(m3[y2:y2+2,x2:x2+2])/4
        result[y2+1, x2+1]=np.sum(m4[y2:y2+2,x2:x2+2])/4

print(result)