def test_score(ind):
    return sum(ind)

if __name__ == '__main__':
    import Exploration.Evolution.Multithreaded_Evolution as Evo
    evolution = Evo.Multithreaded_Evolution(test_score, 60, thread_count=10, name="RL_EVO", mutation=0.05, death_rate=0.3)
    evolution.start([[0.5, 1.0, 0.001, 0.05]])
    #evolution.continue_evo('RL_Test.txt')