from __future__ import unicode_literals
import sys
import random
import matplotlib
matplotlib.use('Qt5Agg')
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QLabel, QSlider, QCheckBox, QLineEdit

from numpy import arange, sin, pi
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from X_Experimental.Functions import *
from multiprocessing import Process, Queue, Value
import time
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg
from PyQt5.QtCore import Qt

from X_Experimental.pattern_generator import *

xmax_param=Value('d', 150)
ymax_param=Value('d', 300)
bin_param=Value('d', 30)

exc_input_nr=Value('d', 100)
inh_input_nr=Value('d', 10)

exc_input_Hz=Value('d', 20)
inh_input_Hz=Value('d', 20)

exc_weights=Value('d', 2)#0.04
inh_weights=Value('d', 0.1)#0.01

rest = Value('d', -60)
reset = Value('d', -60)
threshold = Value('d', -57)
exc_rest = Value('d', 0.0)
inh_rest = Value('d', -80)
tau = Value('d', 20)
tau_e = Value('d', 3)
tau_i = Value('d', 5)

timesteps = Value('d', 1000)
neurons = Value('d', 30)

smooth_param = Value('d', 2)

a_total_max = Value('d', 0.4)
learning_rate = Value('d', 1.0)

def get_smoothed_Distribution(spike_train):
    isi = SpikeTrain_ISI(spike_train, step_to_ms=False)
    isi = np.sort(isi)
    for i in range(int(smooth_param.value)):
        isi = smooth(isi)

    isi_hz = ISI_Hz(isi)
    return isi_hz

def f(q, img_q, timesteps, rest, reset, threshold, exc_rest, inh_rest, tau, tau_e, tau_i, exc_input_nr, inh_input_nr, exc_input_Hz, inh_input_Hz, exc_weights, inh_weights, smooth_param, a_total_max,learning_rate):
    old_ts=-1

    while True:
        if q.empty():
            start = time.time()

            if old_ts != int(timesteps.value):
                old_ts = int(timesteps.value/50)*50
                excitatory_input_streams_source = np.array(get_combined_pattern_timeline_array(100, 10, 10, old_ts, 50))

            #excitatory_input_streams_source *

            #print(excitatory_input_streams.shape)
            excitatory_input_streams =  get_input_stream(old_ts, nr=int(exc_input_nr.value), rate_Hz=exc_input_Hz.value)
            #excitatory_input_streams = correlate(excitatory_input_streams)
            #weighted_excitatory_input_streams = get_weighted_input_stream3(excitatory_input_streams, weight_mul=exc_weights.value)

            inhibitory_input_streams = get_input_stream(old_ts, nr=int(inh_input_nr.value), rate_Hz=inh_input_Hz.value)
            weighted_inhibitory_input_streams = get_weighted_input_stream(inhibitory_input_streams, weight_mul=inh_weights.value)

            #exc_input = np.sum(weighted_excitatory_input_streams, axis=1)
            inh_input = np.sum(weighted_inhibitory_input_streams, axis=1)

            #glu_levels = SpikeTrain_Glutamate(exc_input, tau_e.value)
            gaba_levels = SpikeTrain_GABA(inh_input, tau_i.value)
            #volt_levels, spike_train = InputStream_Voltage(glu_levels, gaba_levels, rest.value,reset.value,threshold.value,exc_rest.value,inh_rest.value,tau.value)

            weights = np.random.rand(int(exc_input_nr.value))
            #weights[0:10]=10
            volt_levels, spike_train, weights2 = InputStream_Voltage_learning(gaba_levels, excitatory_input_streams, weights.copy(), tau_e=3, rest=rest.value, reset=reset.value, threshold=threshold.value, exc_rest=exc_rest.value, inh_rest=inh_rest.value, tau=tau.value, taupre=20, taupost=20, Aplus=0.0003*learning_rate.value, Aminus=-0.0001*learning_rate.value,ATotalMax = a_total_max.value)

            #avg=get_sliding_average_stream(spike_train,100,0)

            print(time.time()-start)
            q.put(spike_train)

            img_q.put([weights.reshape((10, int(len(weights) / 10))), weights2.reshape((10, int(len(weights2) / 10)))])
            #q.put(isi)

#x=step_ms(np.array(range(1000)))

cores=3

class Plot_Surface(QtGui.QWidget):
    #results = []

    def __init__(self, queues):
        super().__init__()
        self.queues = queues

        #timer = QtCore.QTimer(self)
        #timer.timeout.connect(self.update)
        #timer.start(10)


        self.setLayout(QtGui.QVBoxLayout())
        self.canvas = pg.GraphicsLayoutWidget()
        self.layout().addWidget(self.canvas)

        self.otherplot = self.canvas.addPlot(row=0, col=0)

    #def read_queues(self):
    #    for q in self.queues:
    #        try:
    #            while not q.empty():
    #                isi_hz = q.get(timeout=1)
    #                self.results.append(list(isi_hz))
    #        except Exception as error:
    #            pass
    #
    #    max_datapoints=int(neurons.value)
    #
    #    if len(self.results) > max_datapoints:
    #        for i in range(len(self.results) - max_datapoints):
    #            self.results.pop(0)

    def update(self, frequencies, img):

        self.otherplot.setXRange(0, int(xmax_param.value), padding=0)
        self.otherplot.setYRange(0, int(ymax_param.value), padding=0)

        #self.read_queues()


        xrightlim = int(xmax_param.value)
        binwidth = xrightlim / int(bin_param.value)
        bins = np.arange(0, xrightlim, binwidth)

        ## make interesting distribution of values
        #vals = np.hstack([np.random.normal(size=500), np.random.normal(size=260, loc=4)])

        self.otherplot.clear()

        if img is not None:
            #img = np.random.normal(size=(100, 100))
            item = pg.ImageItem(img[0])
            item.scale(2, 6)
            item.setPos(10, 10)
            self.otherplot.addItem(item)

            # img = np.random.normal(size=(100, 100))
            item2 = pg.ImageItem(img[1])
            item2.scale(2, 6)
            item2.setPos(10, 110)
            self.otherplot.addItem(item2)

        #for result in self.results:
        data = [item for sublist in frequencies for item in sublist]


        if len(data)>0:
            y, x = np.histogram(data, bins=bins)
            curve = pg.PlotCurveItem(x, y, stepMode=True, fillLevel=0, brush=(0, 0, 255, 80))
            self.otherplot.addItem(curve)

            mean = np.mean(data)
            std = np.std(data)

            self.mean_plus_std = pg.InfiniteLine(angle=90, pos=mean + std, movable=False, pen=[0, 255, 0, 255])
            self.otherplot.addItem(self.mean_plus_std, ignoreBounds=True)

            self.mean_minus_std = pg.InfiniteLine(angle=90, pos=mean - std, movable=False, pen=[0, 255, 0, 255])
            self.otherplot.addItem(self.mean_minus_std, ignoreBounds=True)

            self.mean_line = pg.InfiniteLine(angle=90, pos=mean, movable=False, pen=[255, 0, 0, 255])
            self.otherplot.addItem(self.mean_line, ignoreBounds=True)



class Plot_Surface2(Plot_Surface):
    def update(self):
        self.read_queues()
        #curve = pg.PlotCurveItem(self.results[0], brush=(0, 0, 255, 80))
        #self.otherplot.addItem(curve)

class ApplicationWindow(QtWidgets.QMainWindow):

    slider_resolution=1000

    def set_slider(self, slider, value):
        slider.setValue(value*self.slider_resolution)
        slider.label.setText(slider.text + ' {:.3f}'.format(value))
        slider.target.value = value

    def on_released(self):
        slider=self.sender()
        current_value = slider.value() / self.slider_resolution
        slider.target.value = current_value
        slider.label.setText(slider.text+' {:.3f}'.format(current_value))

    def add_slider_label(self, box_layout, name, min, max, target, add_onclick=True):

        label=QLabel()
        label.setText(name)

        slider = QSlider(Qt.Horizontal)
        slider.setMinimum(min*self.slider_resolution)
        slider.setMaximum(max*self.slider_resolution)
        val=min
        if target is not None:
            val=target.value
        slider.setValue(val * self.slider_resolution)
        #slider.resize(self.sidebar_width, 5)
        if add_onclick:
            slider.valueChanged.connect(self.on_released)

        slider.target = target
        slider.label=label
        slider.text=name

        label.setText(slider.text + ' {}'.format(val))

        box_layout.addWidget(label)
        box_layout.addWidget(slider)

        return slider

    def space(self, layout, number):
        for i in range(number):
            layout.addWidget(QLabel())

    def __init__(self):


        self.queues = []
        self.avg_queues = []
        self.processes = []
        for i in range(cores):
            q = Queue()
            self.img_q = Queue()
            p = Process(target=f, args=(q, self.img_q, timesteps, rest, reset, threshold, exc_rest, inh_rest, tau, tau_e, tau_i,exc_input_nr, inh_input_nr, exc_input_Hz, inh_input_Hz, exc_weights, inh_weights, smooth_param,a_total_max,learning_rate))
            p.start()
            self.queues.append(q)
            self.processes.append(p)


        QtWidgets.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.init_QT_Window('test')
        self.main_widget = QtWidgets.QWidget(self)

        h=QtWidgets.QHBoxLayout(self.main_widget)


        l = QtWidgets.QVBoxLayout(self)
        self.add_slider_label(l, 'smoothing', 0, 10, smooth_param)

        self.add_slider_label(l, 'histogram bins', 2, 100, bin_param)
        self.add_slider_label(l, 'plot x range', 1, 1000, xmax_param)
        self.add_slider_label(l, 'plot y range', 1, 10000, ymax_param)

        self.add_slider_label(l, 'visible neurons', 1, 1000, neurons)
        self.add_slider_label(l, 'timesteps per neuron', 100, 60000, timesteps)

        self.space(l, 1)

        self.add_slider_label(l, 'a_total_max', 0.01, 10, a_total_max)
        self.add_slider_label(l, 'learning_rate', 0.1, 50, learning_rate)


        self.add_slider_label(l, 'excitatory inputs', 1, 100, exc_input_nr)
        self.add_slider_label(l, 'inhibitory inputs', 1, 100, inh_input_nr)
        self.add_slider_label(l, 'exc. rate', 1, 100, exc_input_Hz)
        self.add_slider_label(l, 'inh. rate', 1, 100, inh_input_Hz)
        self.exc_weights=self.add_slider_label(l, 'exc. weight (rand()*x)/n_inp', 0.01, 10, exc_weights)
        self.add_slider_label(l, 'inh. weight (rand()*x)/n_inp', 0.01, 10, inh_weights)

        h.addLayout(l, stretch=1)
        l = QtWidgets.QVBoxLayout(self)


        self.add_slider_label(l, 'resting potential', -80, -40, rest)
        self.reset_slider=self.add_slider_label(l, 'reset potential', -80, -40, reset)
        self.threshold_slider= self.add_slider_label(l, 'firing threshold', -80, -40, threshold)
        self.add_slider_label(l, 'Glutamate resting potential', -50, 50, exc_rest)
        self.add_slider_label(l, 'GABA resting potential', -100, -60, inh_rest)

        self.space(l, 2)

        self.add_slider_label(l, 'tau Voltage', 0.1, 40, tau)
        self.add_slider_label(l, 'tau Glutamate', 0.1, 40, tau_e)
        self.add_slider_label(l, 'tau GABA', 0.1, 40, tau_i)

        self.space(l, 4)

        self.sl1=self.add_slider_label(l, 'mean', -1, 1, None, False)
        self.sl2=self.add_slider_label(l, 'std', -1, 1, None, False)
        self.sl1.valueChanged.connect(self.sl1event)
        self.sl2.valueChanged.connect(self.sl2event)

        self.info_label1=QLabel()
        self.info_label1.setText('test')
        l.addWidget(self.info_label1)

        self.info_label2=QLabel()
        self.info_label2.setText('test')
        l.addWidget(self.info_label2)

        self.info_label3=QLabel()
        self.info_label3.setText('test')
        l.addWidget(self.info_label3)

        self.cb1 = QCheckBox('Auto Regulate', self)
        l.addWidget(self.cb1)

        self.first_momentum_target_textbox = QLineEdit(self)
        self.first_momentum_target_textbox.setText('0.04')
        l.addWidget(self.first_momentum_target_textbox)

        self.second_momentum_target_textbox = QLineEdit(self)
        self.second_momentum_target_textbox.setText('0.0001')
        l.addWidget(self.second_momentum_target_textbox)

        #self.cb1 = QCheckBox('Show title', self)
        #l.addWidget(self.info_label3)

        #self.my_canvas2 = Plot_Surface2(self.avg_queues)
        #l.addWidget(self.my_canvas2, stretch=6)



        h.addLayout(l, stretch=1)

        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update)
        timer.start(100)


        self.my_canvas = Plot_Surface(self.queues)
        h.addWidget(self.my_canvas, stretch=6)

        self.setCentralWidget(self.main_widget)

    sliding_average_stream=[]
    distribution_stream=[]

    image = None

    def update(self):

        while not self.img_q.empty():
            self.image = self.img_q.get(timeout=1)

        for q in self.queues:
            while not q.empty():
                spike_train = q.get(timeout=1)
                self.sliding_average_stream.append(get_sliding_average_stream(spike_train))
                self.distribution_stream.append(get_smoothed_Distribution(spike_train))

        max_datapoints=int(neurons.value)

        if len(self.sliding_average_stream) > max_datapoints:
            for i in range(len(self.sliding_average_stream) - max_datapoints):
                self.sliding_average_stream.pop(0)

        if len(self.distribution_stream) > max_datapoints:
            for i in range(len(self.distribution_stream) - max_datapoints):
                self.distribution_stream.pop(0)

        #print(self.distribution_stream)

        self.my_canvas.update(self.distribution_stream, self.image)

        div = 0
        avg1 = 0
        avg2 = 0
        combined = 0
        for avg_spike_train in self.sliding_average_stream:
            lst = len(avg_spike_train)
            if lst > 0:
                div += 1
                # spike_train/=len(volt_levels)
                avg1_temp = np.average(avg_spike_train)
                avg2_temp = np.average(np.power(avg_spike_train, 2))
                combined_temp = (avg2_temp - np.power(avg1_temp, 2))

                avg1 += avg1_temp
                avg2 += avg2_temp
                combined += combined_temp

        print(combined)
        if div>0:
            avg1 = avg1/div
            avg2 = avg2/div
            combined = combined/div
            self.info_label1.setText('A: {:.6f} = avg(sliding_average)'.format(avg1))
            self.info_label2.setText('B: {:.6f} = avg(sliding_average^2)'.format(avg2))
            self.info_label3.setText('C: {:.6f} = B-A^2'.format(combined))

            if self.cb1.checkState():
                try:
                    self.first_momentum_target_textbox.value = float(self.first_momentum_target_textbox.text())
                    self.second_momentum_target_textbox.value = float(self.second_momentum_target_textbox.text())
                except:
                    print('warning wrong value')

                mean_error=(avg1-self.first_momentum_target_textbox.value)
                std_error=(combined-self.second_momentum_target_textbox.value)

                #threshold_add=(mean_error*5)#+(2.1*std_error)
                weight_add=-(mean_error*0.5)#+(2.1*std_error)
                reset_add=-(std_error*1000)#(-3*mean_error)#+(5*std_error)

                #self.set_slider(self.threshold_slider, self.threshold_slider.target.value + threshold_add)
                self.set_slider(self.exc_weights, self.exc_weights.target.value + weight_add)
                self.set_slider(self.reset_slider, self.reset_slider.target.value + reset_add)




    def sl1event(self):
        value=self.sl1.value()/self.slider_resolution

        th_add = 2.1*value
        reset_add = 5*value

        self.set_slider(self.reset_slider, -63 + reset_add)
        self.set_slider(self.threshold_slider, -58.5+th_add)

        self.sl1.label.setText(self.sl1.text+' {}'.format(value))


    def sl2event(self):
        value=self.sl2.value()/self.slider_resolution

        th_add = -5*value
        reset_add = 10*value

        self.set_slider(self.reset_slider, -63 + reset_add)
        self.set_slider(self.threshold_slider, -58.5 + th_add)

        self.sl2.label.setText(self.sl2.text + ' {}'.format(value))


    def init_QT_Window(self, label):
        self.width = 1500
        self.height = 500
        self.sidebar_width = 300
        self.btn_height = 20
        self.setWindowTitle(label)
        self.setGeometry(10, 10, self.width, self.height)

    def closeEvent(self, *args, **kwargs):
        for process in self.processes:
            process.terminate()

if __name__ == '__main__':
    qApp = QtWidgets.QApplication(sys.argv)

    aw = ApplicationWindow()
    aw.show()
    sys.exit(qApp.exec_())