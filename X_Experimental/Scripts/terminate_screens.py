import subprocess

identifier='evolution_screen'

#######search for instances

commands = "screen -ls"
output = subprocess.run(["ssh", "vieth@poppy.fias.uni-frankfurt.de", "-t", commands], stdout=subprocess.PIPE).stdout.decode('utf-8')

instances=[]

for line in output.split('\n'):
    if identifier in line:
        data=line.replace(' ','').replace('\t','').split('(')[0]
        instances.append(data)

if len(instances)==0:
    print('nothing found')

#######terminate found instances

for i in instances:
    commands = "screen -X -S "+i+" quit"
    output = subprocess.run(["ssh", "vieth@poppy.fias.uni-frankfurt.de", "-t", commands], stdout=subprocess.PIPE).stdout.decode('utf-8')
    print(i, 'terminated')

