import matplotlib.pyplot as plt
import subprocess

specific=''
specific=' evo_simple_03_9282.txt'
#specific=' evo_all_param_grammar_score_685'
#specific=' evo_all_param_grammar_score_5047'

output=subprocess.run(["ssh", "vieth@poppy.fias.uni-frankfurt.de", "-t", "cd Documents/tren2/Exploration/Evolution/; python3 Evo_Plots.py"+specific], stdout=subprocess.PIPE).stdout.decode('utf-8')

print(output)

y=[]

for line in output.split('\n'):
    data=line.split(' ')
    if len(data)==2 and data[0].isdigit() and data[1].replace('.','').replace('-','').isdigit():
        y.append(float(data[1]))

plt.plot(y)
plt.show()