import subprocess

main_command='srun --partition=sleuths --reservation triesch-shared --mem=32000 --cpus-per-task=32 python3 OscillationTest_new3.py'
#main_command='python3 OscillationTest_new3.py'

commands = 'cd Documents/tren2/Testing/SORN/; screen -dmS evolution_screen sh; screen -S evolution_screen -X stuff "'+main_command+'\r\n"'
output = subprocess.run(["ssh", "vieth@poppy.fias.uni-frankfurt.de", "-t", commands], stdout=subprocess.PIPE).stdout.decode('utf-8')
print(output)