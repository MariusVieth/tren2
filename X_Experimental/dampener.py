import matplotlib.pyplot as plt
import numpy as np


gap=0

for _ in range(1):
    x = []
    y = []
    target = 0
    position = 0

    for i in range(1000):
        position = position + np.random.rand() * 10 - 5  #
        if target - gap - position > 0:
            position = position + (target -gap - position) / 2
        if target + gap - position < 0:
            position = position + (target + gap - position) / 2
        x.append(i)
        y.append(position)

    for z in range(10):
        yi = y.copy()
        for i,_ in enumerate(y[1:-1]):
            y[i]=(yi[i-1]+yi[i]+yi[i+1])/3

    plt.plot(x,y)

plt.axhline(y=gap)
plt.axhline(y=-gap)
plt.show()
