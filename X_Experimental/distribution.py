import matplotlib.pyplot as plt
import numpy as np

def sm(smoothing):
    result = 0
    for _ in range(smoothing):
        result += np.random.rand(10000)

    result = result / smoothing
    return result

def gs(x):
    return np.random.normal(0.5, x*0.003, 10000)

#plt.hist(sm(1), bins=30, alpha=0.5, label='sum_1(rnd*(max-min)-min)/1')
#plt.hist(sm(2), bins=30, alpha=0.5, label='sum_2(rnd*(max-min)-min)/2')
#plt.hist(sm(5), bins=30, alpha=0.5, label='sum_5(rnd*(max-min)-min)/5')
#plt.hist(sm(15), bins=30, alpha=0.5, label='sum_15(rnd*(max-min)-min)/15')
#plt.hist(sm(30), bins=30, alpha=0.5, label='sum_30(rnd*(max-min)-min)/30')

plt.hist(gs(30), bins=30, alpha=0.5, label='normal(0.5, 30*0.003)')
plt.hist(gs(15), bins=30, alpha=0.5, label='normal(0.5, 15*0.003)')
plt.hist(gs(5), bins=30, alpha=0.5, label='normal(0.5, 5*0.003)')
plt.hist(gs(2), bins=30, alpha=0.5, label='normal(0.5, 2*0.003)')
plt.hist(gs(1), bins=30, alpha=0.5, label='normal(0.5, 1*0.003)')





#plt.hist(np.random.normal(0.5,0.1,10000),bins=100)

plt.legend(loc='best', frameon=False, fontsize=20)
plt.xlabel('', fontsize=20)
plt.ylabel('', fontsize=20)

plt.show()