from __future__ import unicode_literals

import matplotlib

matplotlib.use('Qt5Agg')
from PyQt5 import QtWidgets
from multiprocessing import Process, Queue, Value
from pyqtgraph.Qt import QtGui
from PyQt5.QtCore import Qt
from Exploration.UI.Old.Parameter_Exploration_UI import *
from X_Experimental.Functions import *

cores=3


weights = Value('d', 5)
scale = Value('d', 1)
pre_mul = Value('d', 1)
exponent = Value('d', 1)
post_mul = Value('d', 1)
firetreshold = Value('d', 0.3)
stephight = Value('d', 0.0)

act_mul = Value('d', 0.0)


def f2(q,firetreshold,weights,scale,pre_mul,exponent,post_mul,stephight,act_mul ):
    steps=10000
    LGN_PC_Neurons = get_default_Input_Pattern_Neurons(input_width=10, input_height=10, preprocessing_steps=steps)
    excitatory_input_streams = get_TREN_input_stream(steps, LGN_PC_Neurons[TRENNeuronActivator])

    while True:
        if q.empty():
            weighted_excitatory_input_streams = np.sum(get_weighted_input_stream2(excitatory_input_streams, weight_mul=weights.value, scale=scale.value), axis=1)#exc_weights.value

            output_stream = get_TREN_output_stream2(weighted_excitatory_input_streams, pre_mul.value, exponent.value, post_mul.value, firetreshold.value, stephight.value,act_mul.value)
            #q.put(GammaBlock_to_Hz(output_stream))
            q.put(output_stream)

def f(q, firetreshold,weights,scale,pre_mul,exponent,post_mul,stephight,act_mul):
    training_steps = 100000

    neu_rec = TRENNeuronRecorder(['activity'])  # activity

    network, _, _, _ = get_default_Representation_Extraction_Network({
        0: TRENNeuronDimension(1, 1, 1),
        1: InterGammaGlutamate(),
        3: IntraGammaGABA(),
        4: ActivityBuffering(firetreshold=firetreshold.value),
        5: STDP(pre_learn_value=pre_mul.value, post_learn_value=post_mul.value, exponent=exponent.value),  # post_learn_value=0.0005
        6: TemporalWeightCache(decay=1, strength=1),
        8: GlutamateCacheConvergeAndNormalization(norm_value=weights), # =4.5)
        9: neu_rec
    }, preprocessing_steps=100000)


    while True:
        if q.empty():
            network.reset()
            network.simulate_iterations(100, training_steps / 100, True)
            r = []
            for act in neu_rec.activity:
                r.append(GammaBlock_to_Hz(act[0]))
            q.put(r)


class Plot_Surface(QtGui.QWidget):
    results = []

    def __init__(self, queues):
        super().__init__()
        self.queues = queues

        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update)
        timer.start(30)

        self.setLayout(QtGui.QVBoxLayout())
        self.canvas = pg.GraphicsLayoutWidget()
        self.layout().addWidget(self.canvas)

        self.otherplot = self.canvas.addPlot(row=0, col=0)


    def update(self):

        for q in self.queues:
            while not q.empty():
                self.results.append(q.get(timeout=1))

        max_datapoints=5

        if len(self.results) > max_datapoints:
            for i in range(len(self.results) - max_datapoints):
                self.results.pop(0)


        xrightlim = 1
        binwidth = xrightlim / 180
        bins = np.arange(0, xrightlim, binwidth)


        self.otherplot.clear()


        data=[item for sublist in self.results for item in sublist]

        if len(data)>0:

            y, x = np.histogram(data, bins=bins)
            curve = pg.PlotCurveItem(x, y, stepMode=True, fillLevel=0, brush=(0, 0, 255, 80))
            self.otherplot.addItem(curve)

            mean=np.mean(data)


            self.mean_plus_std = pg.InfiniteLine(angle=90, pos=mean, movable=False, pen=[0,255,0,255])
            self.otherplot.addItem(self.mean_plus_std, ignoreBounds=True)


class ApplicationWindow(QtWidgets.QMainWindow):

    slider_resolution=1000

    def on_released(self):
        slider=self.sender()
        current_value = slider.value() / self.slider_resolution
        slider.target.value = current_value
        slider.label.setText(slider.text+' {:.3f}'.format(current_value))

    def add_slider_label(self, box_layout, name, min, max, target):

        label=QLabel()
        label.setText(name)

        slider = QSlider(Qt.Horizontal)
        slider.setMinimum(min*self.slider_resolution)
        slider.setMaximum(max*self.slider_resolution)
        slider.setValue(target.value * self.slider_resolution)
        #slider.resize(self.sidebar_width, 5)
        slider.valueChanged.connect(self.on_released)

        slider.target = target
        slider.label=label
        slider.text=name

        label.setText(slider.text + ' {}'.format(target.value))

        box_layout.addWidget(label)
        box_layout.addWidget(slider)

    def space(self, layout, number):
        for i in range(number):
            layout.addWidget(QLabel())

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.init_QT_Window('test')
        self.main_widget = QtWidgets.QWidget(self)

        h=QtWidgets.QHBoxLayout(self.main_widget)


        self.queues = []
        self.processes = []
        for i in range(cores):
            q = Queue()
            p = Process(target=f2, args=(q, firetreshold,weights,scale,pre_mul,exponent,post_mul,stephight,act_mul,))
            p.start()
            self.queues.append(q)
            self.processes.append(p)


        l = QtWidgets.QVBoxLayout(self)
        self.add_slider_label(l, 'threshold', 0.01, 1, firetreshold)
        self.add_slider_label(l, 'weights', 1, 10, weights)
        self.add_slider_label(l, 'scale', 0.1, 10, scale)
        self.add_slider_label(l, 'pre_mul', -10, 10, pre_mul)
        self.add_slider_label(l, 'exponent', -10, 20, exponent)
        self.add_slider_label(l, 'post_mul', -10, 50, post_mul)
        self.add_slider_label(l, 'stephight', -10, 50, stephight)
        self.add_slider_label(l, 'act_mul', 0.0, 1.0, act_mul)


        h.addLayout(l, stretch=1)


        self.my_canvas = Plot_Surface(self.queues)
        h.addWidget(self.my_canvas, stretch=6)

        self.setCentralWidget(self.main_widget)

    def init_QT_Window(self, label):
        self.width = 1500
        self.height = 500
        self.sidebar_width = 300
        self.btn_height = 20
        self.setWindowTitle(label)
        self.setGeometry(10, 10, self.width, self.height)

    def closeEvent(self, *args, **kwargs):
        for process in self.processes:
            process.terminate()

if __name__ == '__main__':
    qApp = QtWidgets.QApplication(sys.argv)

    aw = ApplicationWindow()
    aw.show()
    sys.exit(qApp.exec_())