import numpy as np
import matplotlib.pyplot as plt



data=[]

alpha=1
beta=0.3
gamma=0.3
p=0.3

print(1-beta+p*gamma)
weight = 10
for i in range(10000):

    alpha_rnd = np.random.rand()*alpha
    beta_rnd = np.random.rand()*beta
    gamma_rnd = np.random.rand()*gamma
    p_rnd = np.random.rand()*p

    weight=weight-weight*beta_rnd+p_rnd*(alpha_rnd+gamma_rnd*weight)
    weight=np.clip(weight, 0, 10)
    data.append(weight)


plt.hist(data, bins=50)
plt.xlim((0, 5))
plt.show()