from PIL import Image, ImageDraw, ImageFilter, ImageEnhance
from scipy.ndimage import *
from scipy import signal
import scipy.ndimage.filters as fi

import numpy as np

#import matplotlib.pyplot as plt

from scipy import fftpack

from mpl_toolkits.mplot3d import Axes3D


def log_kenel(sigma):
    size = int(2 * (np.ceil(3 * sigma)) + 1)
    x, y = np.meshgrid(np.arange(-size / 2, size / 2 + 1), np.arange(-size / 2, size / 2 + 1))
    normal = 1 / (2.0 * np.pi * sigma ** 2)
    kernel = ((x ** 2 + y ** 2 - (2.0 * sigma ** 2)) / sigma ** 4) * np.exp(-(x ** 2 + y ** 2) / (2.0 * sigma ** 2)) / normal
    #fig = plt.figure()
    #ax = fig.add_subplot(111, projection='3d')
    #zs = kernel#np.array(fun(np.ravel(X), np.ravel(Y)))
    #ax.plot_surface(x, y, zs)
    #ax.set_xlabel('X Label')
    #ax.set_ylabel('Y Label')
    #ax.set_zlabel('Z Label')
    #plt.show()
    return kernel


pil_image = Image.open('../Data/Images/sample_03.jpg')
pil_image_gray = pil_image.convert('L')

image_rgb = np.array(pil_image)

image_gray=image_rgb[:, :, 0]+image_rgb[:, :, 1]+image_rgb[:, :, 2]

image_rgb=image_rgb.astype(np.float32)

#image_rgb[:, :, 0] = image_rgb[:, :, 0]/np.max(image_rgb[:, :, 0])
#image_rgb[:, :, 1] = image_rgb[:, :, 1]/np.max(image_rgb[:, :, 1])
#image_rgb[:, :, 2] = image_rgb[:, :, 2]/np.max(image_rgb[:, :, 2])

#plt.imshow(image_rgb[:, :, 0], cmap=plt.cm.gist_gray, interpolation='nearest')
#plt.show()

#plt.imshow(image_rgb[:, :, 1], cmap=plt.cm.gist_gray, interpolation='nearest')
#plt.show()

#plt.imshow(image_rgb[:, :, 2], cmap=plt.cm.gist_gray, interpolation='nearest')
#plt.show()

# = kernel.shape[0]
#log = np.zeros_like(image_gray, dtype=float)

kernel=log_kenel(1)
p_kernel=kernel*(kernel>0)
n_kernel=kernel*(kernel<0)

r_p=signal.convolve2d(image_rgb[:, :, 0], p_kernel, boundary='symm', mode='same')
r_n=signal.convolve2d(image_rgb[:, :, 0], n_kernel, boundary='symm', mode='same')

g_p=signal.convolve2d(image_rgb[:, :, 1], p_kernel, boundary='symm', mode='same')
g_n=signal.convolve2d(image_rgb[:, :, 1], n_kernel, boundary='symm', mode='same')

b_p=signal.convolve2d(image_rgb[:, :, 2], p_kernel, boundary='symm', mode='same')
b_n=signal.convolve2d(image_rgb[:, :, 2], n_kernel, boundary='symm', mode='same')

y_p=signal.convolve2d((image_rgb[:, :, 0] + image_rgb[:, :, 1])/2, p_kernel, boundary='symm', mode='same')
y_n=signal.convolve2d((image_rgb[:, :, 0] + image_rgb[:, :, 1])/2, n_kernel, boundary='symm', mode='same')

gray_p=signal.convolve2d(image_gray, log_kenel(1), boundary='symm', mode='same')

r = np.clip(r_p+g_n, 0, None)
g = np.clip(r_n+g_p, 0, None)
b = np.clip(b_p+y_n, 0, None)
y = np.clip(b_n+y_p, 0, None)
w = np.clip(gray_p, 0, None)

plt.imshow(w, cmap=plt.cm.gist_gray, interpolation='nearest')
plt.show()

r /= np.max(r)
g /= np.max(g)
b /= np.max(b)
y /= np.max(y)
w /= np.max(w)

image_rgb[:, :, 0] = (r*1.5+y)/2.5+w
image_rgb[:, :, 1] = (g*1.5+y)/2.5+w
image_rgb[:, :, 2] = b+w

image_rgb=image_rgb/np.max(image_rgb)

plt.imshow(image_rgb, interpolation='nearest')
plt.show()

plt.imshow(1-image_rgb, interpolation='nearest')
plt.show()

#plt.imshow(np.clip(r_p+g_n, 0, None), cmap=plt.cm.gist_gray, interpolation='nearest')
#plt.show()

#plt.imshow(np.clip(r_n+g_p, 0, None), cmap=plt.cm.gist_gray, interpolation='nearest')
#plt.show()

#plt.imshow(np.clip(b_p+y_n, 0, None), cmap=plt.cm.gist_gray, interpolation='nearest')
#plt.show()

#plt.imshow(np.clip(b_n+y_p, 0, None), cmap=plt.cm.gist_gray, interpolation='nearest')
#plt.show()



#LOG=(r_p+g_n)

#plt.imshow((LOG > 0) * LOG, cmap=plt.cm.gist_gray, interpolation='nearest')
#plt.show()

#plt.imshow((LOG < 0) * LOG*-1, cmap=plt.cm.gist_gray, interpolation='nearest')
#plt.show()
